package exceptions;

import java.io.Serializable;

public class GetSpecificBookingException extends RuntimeException implements Serializable {

    public GetSpecificBookingException(String message) {
        super(message);
    }

    public GetSpecificBookingException() {
        super("The operation did not find any available cars.");
    }
}
