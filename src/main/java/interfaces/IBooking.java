package interfaces;

import DTOs.BookingDetails;
import DTOs.Identifiers.BookingIdentifier;
import DTOs.Identifiers.DriverIdentifier;
import DTOs.NewBookingDetails;
import exceptions.AddNewBookingException;
import exceptions.GetSpecificBookingException;
import exceptions.SearchBookingException;
import java.rmi.RemoteException;
import java.util.List;

/**
 * This is an interface used, between the Frontend and the Backend.
 *
 * @since 1.0
 */
public interface IBooking extends java.rmi.Remote {

    /**
     * addNewBooking This method is used to create a new bookingId in the system
     * and database.
     *
     * @param newBookingDetails a bookingDetails DTO object implementation
     * @return BookingDetails a representation of the finished booking object
     * @throws AddNewBookingException thrown if data can't be parsed
     * @throws java.rmi.RemoteException thrown for unhandled errors
     */
    BookingDetails addNewBooking(NewBookingDetails newBookingDetails) throws AddNewBookingException, RemoteException;

    /**
     * Search for a booking via a DriverIdentifier
     *
     * @param driverId a DriverIdentifier DTO object implementation
     * @return List of BookingDetails DTO objects
     * @throws SearchBookingException thrown if data can't be parsed
     * @throws java.rmi.RemoteException thrown for unhandled errors
     */
    List<BookingDetails> searchBooking(DriverIdentifier driverId) throws SearchBookingException, RemoteException;

    /**
     * getSpecificBooking method returns a BookingDetails object for the
     * requested bookingId
     *
     * @param bookingId
     * @return BookingDetails
     * @throws GetSpecificBookingException
     * @throws RemoteException
     */
    BookingDetails getSpecificBooking(BookingIdentifier bookingId) throws GetSpecificBookingException, RemoteException;

    /**
     * cancelBooking This method takes care of marking a booking as cancelled.
     * it needs a bookingId to cancel a booking.
     *
     * @param bookingId a BookingIdentifier DTO object implementation
     * @throws java.rmi.RemoteException thrown for unhandled errors
     * @since 1.0
     */
    void cancelBooking(BookingIdentifier bookingId) throws RemoteException;
}
