package DTOs.Identifiers;

import java.io.Serializable;

/**
 * CarIdentifier This is the data transmission object used for identifying a
 * city
 *
 * @since 1.0
 */
public abstract class CityIdentifier implements Serializable {

    /**
     * Used to specify the id of the city
     *
     * @since 1.0
     */
    private String id;

    public CityIdentifier(String id) {
        this.id = id;
    }

    public String getId() {
        return id;
    }
}
