package DTOs;

import DTOs.Identifiers.DriverIdentifier;
import java.io.Serializable;
import java.util.Date;

public class DriverDetails extends DriverIdentifier implements Serializable {

    private String name;
    private Date age;
    private String driverLicenseNumber;

    public DriverDetails(String id, String name, Date age, String driverLicenseNumber) {
        super(id);
        this.name = name;
        this.age = age;
        this.driverLicenseNumber = driverLicenseNumber;
    }

    public String getName() {
        return name;
    }

    public Date getAge() {
        return age;
    }

    public String getDriverLicenseNumber() {
        return driverLicenseNumber;
    }
}
