package DTOs;

import DTOs.Identifiers.BookingIdentifier;
import DTOs.Identifiers.CarIdentifier;
import DTOs.Identifiers.StationIdentifier;
import java.io.Serializable;

import java.util.Date;

public class BookingSummary extends BookingIdentifier implements Serializable {

    private Date pickUpTime;
    private StationIdentifier pickUpPlace;
    private CarIdentifier car;

    public BookingSummary(Date pickUpTime, StationIdentifier pickUpPlace, CarIdentifier car, String bookingId) {
        super(bookingId);
        this.pickUpTime = pickUpTime;
        this.pickUpPlace = pickUpPlace;
        this.car = car;
    }

    public Date getPickUpTime() {
        return pickUpTime;
    }

    public StationIdentifier getPickUpPlace() {
        return pickUpPlace;
    }

    public CarIdentifier getCar() {
        return car;
    }
}
