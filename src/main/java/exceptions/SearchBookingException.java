package exceptions;

import java.io.Serializable;

public class SearchBookingException extends RuntimeException implements Serializable {

    public SearchBookingException(String message) {
        super(message);
    }

    public SearchBookingException() {
        super("The operation did not find any matching bookings.");
    }
}
