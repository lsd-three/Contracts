/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package TEST;

import java.rmi.Remote;
import java.rmi.RemoteException;

/**
 *
 * @author Orchi
 */
public interface ITest extends Remote {

    public String hello(String from) throws RemoteException;

}
