package DTOs;

import DTOs.Identifiers.BookingIdentifier;
import DTOs.Identifiers.CarIdentifier;
import DTOs.Identifiers.DriverIdentifier;
import DTOs.Identifiers.StationIdentifier;
import java.io.Serializable;
import java.util.Date;

public class BookingDetails extends BookingIdentifier implements Serializable {

    private Date pickUpTime;
    private Date deliveryTime;
    private StationIdentifier pickUpPlace;
    private StationIdentifier deliveryPlace;
    private DriverIdentifier driver;
    private CarIdentifier car;
    private double totalPrice;

    public BookingDetails(Date pickUpTime, Date deliveryTime, StationIdentifier pickUpPlace, StationIdentifier deliveryPlace, DriverIdentifier driver, CarIdentifier car, double totalPrice, String bookingId) {
        super(bookingId);
        this.pickUpTime = pickUpTime;
        this.deliveryTime = deliveryTime;
        this.pickUpPlace = pickUpPlace;
        this.deliveryPlace = deliveryPlace;
        this.driver = driver;
        this.car = car;
        this.totalPrice = totalPrice;
    }

    public Date getPickUpTime() {
        return pickUpTime;
    }

    public Date getDeliveryTime() {
        return deliveryTime;
    }

    public StationIdentifier getPickUpPlace() {
        return pickUpPlace;
    }

    public StationIdentifier getDeliveryPlace() {
        return deliveryPlace;
    }

    public DriverIdentifier getDriver() {
        return driver;
    }

    public CarIdentifier getCar() {
        return car;
    }

    public double getTotalPrice() {
        return totalPrice;
    }
}
