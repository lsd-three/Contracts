package exceptions;

import java.io.Serializable;

public class GetAvailableCarsException extends RuntimeException implements Serializable {

    public GetAvailableCarsException(String message) {
        super(message);
    }

    public GetAvailableCarsException() {
        super("The operation did not find any available cars.");
    }
}
