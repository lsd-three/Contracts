package DTOs;

import DTOs.Identifiers.StationIdentifier;
import java.io.Serializable;
import java.util.Date;

/**
 * Class representing a "Show available rental cars" Data Transfer Object
 *
 */
public class AvailabilityDetails  implements Serializable{

    private Date pickUpTime;
    private Date deliveryTime;
    private StationIdentifier pickUpPlace;

    public AvailabilityDetails(Date pickUpTime, Date deliveryTime, StationIdentifier pickUpPlace) {
        this.pickUpTime = pickUpTime;
        this.deliveryTime = deliveryTime;
        this.pickUpPlace = pickUpPlace;
    }

    public Date getPickUpTime() {
        return pickUpTime;
    }

    public Date getDeliveryTime() {
        return deliveryTime;
    }

    public StationIdentifier getPickUpPlace() {
        return pickUpPlace;
    }

}
