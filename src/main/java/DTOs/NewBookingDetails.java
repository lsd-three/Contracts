package DTOs;

import DTOs.Identifiers.StationIdentifier;
import java.io.Serializable;
import java.util.Date;

public class NewBookingDetails implements Serializable {

    private Date pickUpTime;
    private Date deliveryTime;
    private StationIdentifier pickUpPlace;
    private StationIdentifier deliveryPlace;
    private DriverDetails driver;
    private CarDetails car;

    public Date getPickUpTime() {
        return pickUpTime;
    }

    public void setPickUpTime(Date pickUpTime) {
        this.pickUpTime = pickUpTime;
    }

    public Date getDeliveryTime() {
        return deliveryTime;
    }

    public void setDeliveryTime(Date deliveryTime) {
        this.deliveryTime = deliveryTime;
    }

    public StationIdentifier getPickUpPlace() {
        return pickUpPlace;
    }

    public void setPickUpPlace(StationIdentifier pickUpPlace) {
        this.pickUpPlace = pickUpPlace;
    }

    public StationIdentifier getDeliveryPlace() {
        return deliveryPlace;
    }

    public void setDeliveryPlace(StationIdentifier deliveryPlace) {
        this.deliveryPlace = deliveryPlace;
    }

    public DriverDetails getDriver() {
        return driver;
    }

    public void setDriver(DriverDetails driver) {
        this.driver = driver;
    }

    public CarDetails getCar() {
        return car;
    }

    public void setCar(CarDetails car) {
        this.car = car;
    }

}
