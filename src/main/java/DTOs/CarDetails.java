package DTOs;

import DTOs.Identifiers.CarIdentifier;
import java.io.Serializable;

public class CarDetails extends CarIdentifier implements Serializable {

    private CarTypeDetails carType;

    public CarDetails(String id, CarTypeDetails carType) {
        super(id);
        this.carType = carType;
    }

    public CarTypeDetails getCarType() {
        return carType;
    }

    public String getLicensePlate() {
        return getId();
    }
}
