package exceptions;

import java.io.Serializable;

public class AddNewBookingException extends RuntimeException implements Serializable {

    public AddNewBookingException(String message) {
        super(message);
    }

    public AddNewBookingException() {
        super("The operation of adding your booking went wrong.");
    }
}
