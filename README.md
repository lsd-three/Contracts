# Faraday Three Car Rental

## SLA

### Uptime/availability

Based on previous meassurements, we concluded that we can attain > 95% uptime. View live meassurements [here](http://138.68.106.203:3002/dashboard/db/faraday-carrental-backend-and-frontend?refresh=5s&orgId=1) (Credentials are found [here](https://gitlab.com/lsd-three/backend/wikis/Credentials-and-other-info
))

### Mean response time
Based on basic HTTP GET request using CURL from a client in Denmark to our hosted project in Frankfurt,
we guarantee a mean response time < 500ms.

### Mean time to recover
The droplet uptime is not in our scope to estimate, but we believe DigitalOcean will sustain high uptime.

Regarding the applications(s), they should be up in 6 seconds on application crash. Our reboot service is set to restart in 5 second intervals. 

### Failure frequency
We aim to keep the failure frequency low, as we dont have sufficient data to get a more precise failure frequency estimation.

---

Download the latest jar file here https://gitlab.com/lsd-three/Contracts/-/jobs/artifacts/master/download?job=maven-build


## Artifacts used, including diagrams
[Artifacts](https://gitlab.com/lsd-three/Artifacts)

## Team Three

### Frontend

*Oliver Scholz Lønning*

*Yosuke Ueda*

*Benjamin Schultz Larsen*

*Jacob Sørensen*

*Mathias Brinkmann Igel* 

### Backend

*Mathias Bigler*

*Elitsa Miroslavova Marinovska*

*Stanislav Novitski*

*Alexander Winther Hørsted-Andersen*

*David de Lima Fraga Alves* 
